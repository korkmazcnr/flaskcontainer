#!/bin/sh
#ck: APP_SCRIPT environment variable, Used to run the application from a script file. 
#This should be a path to a script file (defaults to app.sh unless set to null) that will be run to start the application.
#https://github.com/sclorg/s2i-python-container/tree/master/3.8#environment-variables

export FLASK_APP=helloAdv.py && flask run --host=0.0.0.0 --port=8080 