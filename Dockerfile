FROM registry.access.redhat.com/ubi8/python-38

# Add application sources with correct permissions for OpenShift
USER 0
#ADD app-src .
#ck: add everything in current context to the current directory in image
ADD . .
RUN chown -R 1001:0 ./
USER 1001

# Install the dependencies
RUN pip install -U "pip>=19.3.1" && \
    pip install -r requirements.txt
    #python manage.py collectstatic --noinput && \
    #python manage.py migrate

#ck:
EXPOSE 5000/tcp

# Run the application
#CMD python manage.py runserver 0.0.0.0:8080
#CMD export FLASK_APP=helloAdv.py && flask run
#ck: Externally Visible Server by adding "--host=0.0.0.0" parameter
#CMD export FLASK_APP=helloAdv.py && flask run --host=0.0.0.0 --port=8080
CMD export FLASK_APP=helloAdv.py && flask run --host=0.0.0.0